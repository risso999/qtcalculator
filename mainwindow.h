#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString doOperation();
    void addNumber(QString button);
    void doMath(QString opr);

private slots:
    void on_Button_clear_released();

    void on_Button_back_released();

    void on_Button_percent_released();

    void on_Button_negative_released();

    void on_Button_point_released();

    void on_Button_0_released();

    void on_Button_1_released();

    void on_Button_2_released();

    void on_Button_3_released();

    void on_Button_4_released();

    void on_Button_5_released();

    void on_Button_6_released();

    void on_Button_7_released();

    void on_Button_8_released();

    void on_Button_9_pressed();

    void on_Button_plus_released();

    void on_Button_is_released();

    void on_Button_minus_released();

    void on_Button_multiply_released();

    void on_Button_divide_released();

private:
    Ui::MainWindow *ui;
    QString operation;
    QString operand1;
    QString operand2;
    bool clear;
    bool equal;
};

#endif // MAINWINDOW_H
