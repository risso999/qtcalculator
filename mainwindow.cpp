#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    operation = "";
    operand1 = "0";
    operand2 = "";
    clear = false;
    equal = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_Button_clear_released()
{
    ui->display->setText("0");
    operation = "";
    operand1 = "0";
    operand2 = "";
    clear = false;
    equal = false;
}


void MainWindow::on_Button_back_released()
{
    QString value = ui->display->text();
    QString number = value;
    number.remove(QChar('-'));
    if (number.length() == 1) {
        ui->display->setText("0");
    }
    else {
        ui->display->setText(value.left(value.length() - 1));
    }
}


void MainWindow::on_Button_percent_released()
{
    QString value = ui->display->text();
    ui->display->setText(QString::number(value.toDouble() / 100));
}


void MainWindow::on_Button_negative_released()
{
    QString value = ui->display->text();
    if (value.startsWith(QChar('-'))){
        ui->display->setText(value.remove(QChar('-')));
    }
    else {
        ui->display->setText(QString("-") + value);
    }
}


void MainWindow::on_Button_point_released()
{
    QString value = ui->display->text();
    if (clear) {
        clear = false;
        ui->display->setText("0.");
    }
    else if (!value.contains(QChar('.'))){
        ui->display->setText(value + QString("."));
    }
}


void MainWindow::addNumber(QString button)
{
    QString value = ui->display->text();
    QString number = value;
    number.remove(QChar('-'));
    if (clear) {
        clear = false;
        ui->display->setText(button);
    }
    else if (number == "0"){
        ui->display->setText(button);
    }
    else{
        ui->display->setText(value + button);
    }
}


void MainWindow::on_Button_0_released()
{
    this->addNumber("0");
}


void MainWindow::on_Button_1_released()
{
    this->addNumber("1");
}


void MainWindow::on_Button_2_released()
{
    this->addNumber("2");
}


void MainWindow::on_Button_3_released()
{
    this->addNumber("3");
}


void MainWindow::on_Button_4_released()
{
    this->addNumber("4");
}


void MainWindow::on_Button_5_released()
{
    this->addNumber("5");
}


void MainWindow::on_Button_6_released()
{
    this->addNumber("6");
}


void MainWindow::on_Button_7_released()
{
    this->addNumber("7");
}


void MainWindow::on_Button_8_released()
{
    this->addNumber("8");
}


void MainWindow::on_Button_9_pressed()
{
    this->addNumber("9");
}

QString MainWindow::doOperation(){
    if (operand1 == "Error" || operand2 == "Error") {
        return QString("Error");
    }
    else if (operation == "+"){
        return QString::number(operand1.toDouble() + operand2.toDouble());
    }
    else if (operation == "-"){
        return QString::number(operand1.toDouble() - operand2.toDouble());
    }
    else if (operation == "*"){
        return QString::number(operand1.toDouble() * operand2.toDouble());
    }
    else if (operation == "/"){
        if (operand2.toDouble() == 0){
            return QString("Error");
        }
        else {
            return QString::number(operand1.toDouble() / operand2.toDouble());
        }
    }
    else {
        return ui->display->text();
    }
}

void MainWindow::doMath(QString opr){
    if (operation != "" && !clear && !equal){
        operand2 = ui->display->text();
        operand1 = this->doOperation();
        ui->display->setText(operand1);
    }
    else {
        operand1 = ui->display->text();
    }
    operation = opr;
    clear = true;
    equal = false;
}

void MainWindow::on_Button_plus_released()
{
    this->doMath("+");
}


void MainWindow::on_Button_minus_released()
{
    this->doMath("-");
}


void MainWindow::on_Button_multiply_released()
{
    this->doMath("*");
}


void MainWindow::on_Button_divide_released()
{
    this->doMath("/");
}


void MainWindow::on_Button_is_released()
{
    if (operation != "") {
        if (!clear){
            operand2 = ui->display->text();
        }
        operand1 = this->doOperation();
        ui->display->setText(operand1);
        clear = true;
        equal = true;
    }
}

