# QtCalculator

Pomocí softwarové knihovny, vhodného vývojového prostředí a programu pro návrh dialogových oknen vytvořte aplikaci, která bude pracovat jako kalkulátor. Aplikace musí splňovat tyto požadavky:

1) Jednotlivé číslice musí být vkládány pomocí klikání myší na tlačítka kalkulátoru. Stejným způsobem bude voleno znaménko čísla a desetinná tečka.

2) Programu musí umožňovat opravu vloženého čísla, tzn. krok zpět a smazání celé operace.

3) Kalkulátor musí obsahovat tlačítka pro provádění těchto matematických operací: +,-,*,/ (samozřejmě další iniciativě se meze nekladou :)).

4) Kalkulátor si musí umět "zapamatovat" libovolné množství matematických operací zadávaných po sobě. Výsledek výpočtu se na displeji kalkulátoru zobrazí až po stitku tlačítka '='. Pro ukládání dílčích operandů a typů matematických operací použijte jednu nebo více třídy kolekcí knihovny Qt (pole, seznamy, mapy).

5) Kalkulátor by měl podporovat výpočet matematických výrazů se závorkami.
